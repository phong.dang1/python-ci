FROM python:3.10.4-alpine3.15

WORKDIR /opt/

RUN pip install pyinstaller

COPY *.py .

RUN pyinstaller --on-file run.py
RUN cp dist/run /bin
