#!/usr/bin/python

import socket
import paramiko
import requests

def get_ip():
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.connect(("8.8.8.8", 80))
	return s.getsockname()[0]

def get_ip_from_name(hostname):
	return socket.gethostbyname(hostname)

def get_url(url):
	url = url
	# url = 'https://github.com/mach1el?tab=repositories'
	re = requests.get(url)
	return re.content

def get_os_info(ip):
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(ip, username="vagrant", password="vagrant")
	stdin_, stdout_, stderr_ = ssh.exec_command("cat /etc/issue")
	if 'Debian' in stdout_.readlines():
		print("This is Debian server")
	else:
		print("This is Demo server")
