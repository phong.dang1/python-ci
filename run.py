#!/usr/bin/python

import sys
import validators
from tasks import *
from argparse import ArgumentParser,RawTextHelpFormatter

sys.dont_write_bycode=True

def main():
	parser = ArgumentParser(
        usage='./%(prog)s -u [url] ',
        formatter_class=RawTextHelpFormatter,
        prog='run',
        epilog='''
Example:
    ./%(prog)s -u https://github.com/mach1el
'''
)

	options = parser.add_argument_group('options','')
	options.add_argument('-i',metavar='xx.xx.xx.xx',default=None,help='Specify IP')
	options.add_argument('-u',metavar='<http|https://uri>',default=None,help='Specify url')
	options.add_argument('--hostname',metavar='somethings.com',default=None,help='Specify hostname')
	args = parser.parse_args()

	if (args.u == None and args.i == None and args.hostname == None):
		parser.print_help()
		sys.exit()

	else:
		url=args.u
		ip=args.i
		hostname=args.hostname
		
		if url != None:
			if validators.url(url):
				print(get_url(url))
			else:
				url = 'https://'+url
				print(get_url(url))

		if hostname != None:
			print(get_ip_from_name(hostname))

		if ip != None:
			get_os_info(ip)

if __name__ == '__main__':
	main()